import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const UserHeader = ({ user }) => {
  if (!user) return null;

  return <div>{user.name}</div>;
};

const mapStateToProps = ({ users }, { userId }) => {
  const user = users.find(user => user.id === userId);
  return { user };
};

UserHeader.propTypes = {
  user: PropTypes.object,
};

export default connect(mapStateToProps)(UserHeader);
