import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import UserHeader from './UserHeader';
import { fetchPostsAndUsers } from '../actions';

const PostList = ({ fetchPostsAndUsers, posts }) => {
  useEffect(() => {
    fetchPostsAndUsers();
  }, []);

  const renderList = () => {
    return posts.map(({ id, title, body, userId }) => (
      <div className='item' key={id}>
        <i className='large middle aligned icon user' />
        <div className='content'>
          <div className='description'>
            <h2>{title}</h2>
            <p>{body}</p>
          </div>
          <UserHeader userId={userId} />
        </div>
      </div>
    ));
  };

  return <div className='ui relaxed divided list'>{renderList()}</div>;
};

PostList.propTypes = {
  fetchPostsAndUsers: PropTypes.func.isRequired,
  posts: PropTypes.array.isRequired,
};

const mapStateToProps = ({ posts }) => ({ posts });

export default connect(mapStateToProps, {
  fetchPostsAndUsers,
})(PostList);
