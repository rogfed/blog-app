import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware} from "redux"
import thunk from 'redux-thunk'

import reducers from './reducers'
import AppRouter from './routers/AppRouter';

import './styles/app.scss';

ReactDOM.render(
  <Provider store={createStore(reducers, applyMiddleware(thunk))}>
    <AppRouter />
  </Provider>
  , document.getElementById('app'));
